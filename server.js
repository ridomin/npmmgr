const express = require('express')
const app = express()
const loadModelsFromFS = require('./dtFileSystemRepo').loadModelsFromFS

const port = process.env.PORT || 3000

app.get('/', async (req, res) => {
   const models = await loadModelsFromFS()
   res.json(models.map(m=>m.id))
})

var server = app.listen(port,() => console.log("Example app listening %s", port))