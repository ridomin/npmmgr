const fs  = require('fs')
const glob = require('glob')

const loadModelsFromFS = function() {
  const dir = './plugin_packages/@digital-twins';
  const models = []
  return new Promise((resolve,reject) => {
      glob(dir + '/**/package.json', function(err, files) {
        files.forEach(f => {
            const pjson =  JSON.parse(fs.readFileSync(f, 'utf-8'))
            pjson.models.forEach(m=>{
                const dtdlModel = JSON.parse(fs.readFileSync(f.replace('package.json',m),'utf-8'))
                models.push({id: dtdlModel['@id'],dtdlModel})
            })
        })
        resolve(models)
    })
  })
}
module.exports= {loadModelsFromFS}