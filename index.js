'use strict';

const PluginManager = require('live-plugin-manager').PluginManager
const loadModelsFromFS = require('./dtFileSystemRepo').loadModelsFromFS

const manager = new PluginManager()

async function run () {

  const pi = await manager.queryPackageFromNpm('@digital-twins/com-example-thermostat')
  console.log(pi.name + ' ' + pi.version)
  await manager.install(pi.name, pi.version)
  
  const models = await loadModelsFromFS()
  models.forEach(m=>console.log(m.id)) 
}
run()
